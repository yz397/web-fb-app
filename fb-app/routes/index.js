var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
	var status = 'Zhang Yingzhuo';
	if (res.status === 'unknown'){
		status = 'unknown'
	}else if(res.status == 'not_authorized'){
		status = 'not_authorized';
	}else if(res.status === 'connected'){
		status = 'connected';
	}
	res.render('index', { title: status });
});

module.exports = router;
