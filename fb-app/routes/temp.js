function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);
    if (response.status === 'connected') {
//如果已经登陆facebook，则获取信息
        testAPI();
    } else {
//否则，提醒用户登录
        alert('请先登陆facebook！');
        window.open('http://fb.com');
        window.location = window.location.href;
    }
}

function checkLoginState() {
    FB.getLoginStatus(function(response) {
        statusChangeCallback(response);
    });
}

window.fbAsyncInit = function() {
    FB.init({
        appId      : '301449836930353',  //应用编号
        cookie     : true,
        xfbml      : true,
        version    : 'v2.8'
    });
    //登陆方法
FB.getLoginStatus(function(response) {
        statusChangeCallback(response);
    });
};

(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

//获取用户信息
function testAPI() {
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me', function(response) {
       console.log(response.name);
    });
}