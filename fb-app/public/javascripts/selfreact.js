function Welcome(props) {
  return <h1>Hello, {props.name},{props.age}</h1>;
}

function App() {
  return (
    <div>
      <Welcome name="Sara" age="12" />
      <Welcome name="Cahal" age="14"/>
      <Welcome name="Edite" age="35"/>
    </div>
  );
}

ReactDOM.render(<App />, document.getElementById('post'));