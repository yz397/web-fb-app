
var mongoose = require('mongoose');
var db = mongoose.createConnection('mongodb://localhost:27017/test');
var bcrypt = require('bcrypt');

db.on('error', function(error) {
    console.log(error);
});
var Schema = mongoose.Schema;

var SALT = 10;

var userSchema = new Schema({
    username     : {
      unqiue:true,
      type: String
    },
    password : {type: String},
    email:{
      unique: true,
      type: String}
    meta:{
      createAt:{
        type: Date,
        default: Date.now()
      },
      updateAt:{
        type: Date,
        default: Date.now()
      }
    }
});

// userSchema.pre('save',function(next){
//   var user=this;
//   if(this.isNew){
//     this.meta.createAt = this.meta.updateAt = Date.now();
//   }else{
//     this.meta.updateAt = Date.now();
//   }
  
//   bcrypt.genSalt(SALT, function(err,salt){
//     if(err){
//       return next(err);
//     }
//     bcrypt.hash(user.password,salt,function(err,hash){
//       if(err){
//         return next(err);
//       }
//       use.password = hash;
//       next();
//     });
//   });
// });
// userSchema.methods = {
//   comparePassword: function (_password, cb) {
//     bcrypt.compare(_password, this.password, function (err, isMatch) {
//       if (err) {
//         return cb(err);
//       }
//       cb(null, isMatch);
//     })
//   }
// };
// userSchema.statics = {
//   fetch: function (cb) {
//     return this
//       .find({})
//       .sort('meta.updateAt')
//       .exec(cb);
//   },
//   findById: function (id, cb) {
//     return this
//       .findOne({_id: id})
//       .exec(cb)
//   }
// };

exports.user = db.model('admins', userSchema);
exports.db = db;