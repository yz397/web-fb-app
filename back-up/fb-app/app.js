var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var mongoose  = require('mongoose');
mongoose.connect('mongodb://localhost/test');

var app = express();
var db = mongoose.connection;

db.on('open',function(){
  console.log('mongoDB connection successed');
});

db.on('error',function(){
  console.log('mongoDB connection failed');
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false}));

app.get('/',function(req,res){
	res.render('views/login.html');
});
app.listen(8000);